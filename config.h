//
// Created by vlchung on 12/9/21.
//

#ifndef P8WHISTLE_CONFIG_H
#define P8WHISTLE_CONFIG_H

// we are saving a pin by using a voltage divider connected to various analog pins. Each resistor in the divider
// has a push button to "bypass" the resistor. Thus, unpressed the analog reading should be in the middle, and if one
// of the buttons is pressed it goes either low or high.
// Rough schematic is:
// GND->1K->100K->->100k->1K->VCC
//        |     | ||     |
//        Switch  | Switch
//                ^AnalogPin Connects here
// The two 1K resistors are to protect the device if some idiot presses both buttons simultaneously

#define PIN_TEAMA A7
#define PIN_TEAM1 A6
#define PIN_ENDGO A2 // either go to 5 or endS
#define PIN_OTO 14
#define PIN_MP3BUSY A3
#define PIN_MUTEWHISTLE 3
#define PIN_MUTEHORN 4

#define ANALOG_LOW 341
#define ANALOG_HIGH 682

#define ADDR_TIMINGBEACON 0x27

#define PING_INTERVAL 5000
// Have discovered in field tests that no activity for a certain amount of time causes the TCP/IP connection to close.
#define DEBOUNCE_INTERVAL 500

#endif //P8WHISTLE_CONFIG_H
