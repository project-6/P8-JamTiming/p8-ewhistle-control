//
// Created by vlchung on 1/9/21.
//

#ifndef P8WHISTLE_GLOBAL_H
#define P8WHISTLE_GLOBAL_H
#define VLCF(x) F(x)
extern long currentTimeMillis;
extern long lastSerial;
uint16_t dialRead(uint8_t analogChannelNumber, long maxChoices);

void debug(String);
void debug(String, String);
void OUT_SERIAL(String);
void OUT_SERIAL(String, String);
#endif //P8WHISTLE_GLOBAL_H
