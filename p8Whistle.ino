#include <avr/sleep.h>
#include <Arduino.h>
#include <AltSoftSerial.h>
#include <EEPROM.h>
#include "JQ8400_Serial.h"
#include "mp3player.h"
#include "global.h"
#include "SimpleMCP23008.h"
#include "config.h"

long theTime = 0;
bool theState = false;
bool thePlayback = false;

// These refer to button pushes only
long lastOTO = 0, lastTeamA = 0, lastTeam1 = 0, lastEndGo = 0, lastVol = 0, lastVolVol = 30;

String line;
bool lineReady = false;

SimpleMCP23008 timingBeacon(ADDR_TIMINGBEACON);
AltSoftSerial Serial1;
JQ8400_Serial mp3hardware(Serial1);
MP3Player g_mp3Player(&mp3hardware);

enum PlayMode {
    None,
    Single,
    Multiple,
    Counting // technically counting means we're ignoring playmode but leave it here for now
};
char speechQueue[10] = {0};

enum CountType {
    NA,
    All, // previously known as "warn"
    Up,
    Down,
    OneUp,
    OneDown
};

enum AmpActivity {
    Neither,
    Whistle,
    Speak
};

CountType countType = NA;
PlayMode playMode = None;
long countTime = 0;

// Originally the intention was to throw away the cheap amp that came with the horn speaker and use a TDA8932 in
// BTL configuration (see footnote hornAmp.png). Turns out that the cheap amp is much better at driving the
// horn speaker.
//
// The horn speaker amp also contains a relay that cuts power to the amp (presumably to save energy and heat). We
// disconnect the relay's driver transistor's base from the rest of the circuit and drive it directly ourselves
// via a 2.7K resistor in series to the "mute horn" pin. Send the pin HIGH to unmute.


int volumeWhistle = 30;
int volumeSpeak = 30;

bool beaconNoise = false;
// if true, will turn the beacon on/off when making noises.

void beaconCancel() {
    // if we are in the middle of a count, cancel the count. This should fix a theoretically speculated bug where
    // a count will continue even if someone requests a timeout mid-count.
    if (countType != NA) {
        countType = NA;
        timingBeacon.write(0);
    }
    return;
}

void ampControl(AmpActivity mode) {
    // the weird "pinMode = INPUT" is a way of making the pin go into
    // high impedance (and let the internal amp pull-up do it's job),
    // rather than busting its gut to drive the relevant mute pin HIGH.
    switch (mode) {
        case Whistle:
            if (beaconNoise) {
                timingBeacon.write(0x1F);
            }
            g_mp3Player.setVol(volumeWhistle);
            pinMode(PIN_MUTEWHISTLE, INPUT);
            digitalWrite(PIN_MUTEHORN, LOW);
            break;
        case Speak:
            if (beaconNoise) {
                timingBeacon.write(0x1F);
            }
            g_mp3Player.setVol(0);
            pinMode(PIN_MUTEWHISTLE, OUTPUT);
            digitalWrite(PIN_MUTEHORN, HIGH);
            g_mp3Player.setVol(volumeSpeak);
            break;
        case Neither:
            if (beaconNoise) {
                timingBeacon.write(0x0);
                beaconNoise = false;
            }
            pinMode(PIN_MUTEWHISTLE, OUTPUT);
            digitalWrite(PIN_MUTEHORN, LOW);
            break;
    }
    digitalWrite(PIN_MUTEWHISTLE, LOW);
    return;
}


void setup() {
    Serial.begin(38400);
    Serial1.begin(9600);
    timingBeacon.begin();
    timingBeacon.setDDR(0xE0);
    timingBeacon.write(0x00);

    pinMode(PIN_OTO, INPUT_PULLUP);

    pinMode(PIN_MUTEWHISTLE, OUTPUT);
    pinMode(PIN_MUTEHORN, OUTPUT);
    ampControl(Neither);

    int volume = 30;
    uint8_t value = EEPROM.read(0x00);
    if (value != 0xFF) {
        volume = value;
    }
    g_mp3Player.begin(volume, PIN_MP3BUSY);
    volumeWhistle = volume;
    debug(VLCF("VOLW: "), String(volumeWhistle));

    volume = 30;
    value = EEPROM.read(0x01);
    if (value != 0xFF) {
        volume = value;
    }
    volumeSpeak = volume;
    debug(VLCF("VOLS: "), String(volumeSpeak));

    sleep_bod_disable();
    OUT_SERIAL(VLCF("Ready"));
}

const char playlistCalloff[] = "010101";
const char playlistCalloffShort[] = "0101";
const char playlistTO[] = "01";
const char playlistStart[] = "02";
const char playlistEndTO[] = "03";

void catColour(char *destination, String colour) {
    if (colour == "black") {
        strcat(destination, "CA");
        return;
    }
    if (colour == "white") {
        strcat(destination, "CB");
        return;
    }
    if (colour == "green") {
        strcat(destination, "CC");
        return;
    }
    if (colour == "gold") {
        strcat(destination, "CD");
        return;
    }
    if (colour == "red") {
        strcat(destination, "CE");
        return;
    }
    if (colour == "blue") {
        strcat(destination, "CF");
        return;
    }
    if (colour == "yellow") {
        strcat(destination, "CG");
        return;
    }
    if (colour == "maroon") {
        strcat(destination, "CH");
        return;
    }
    if (colour == "pink") {
        strcat(destination, "CI");
        return;
    }
    if (colour == "purple") {
        strcat(destination, "CJ");
        return;
    }
    if (colour == "celeste") {
        strcat(destination, "CK");
        return;
    }
    if (colour == "orange") {
        strcat(destination, "CL");
        return;
    }
    if (colour == "brown") {
        strcat(destination, "CM");
        return;
    }
    if (colour == "teal") {
        strcat(destination, "CN");
        return;
    }
    return;
}

int interpretAnalogPin(int pin) {
    uint16_t analogReading = analogRead(pin);
    // for reading the "2 buttons on 1 analog pin" things:
    if (analogReading < ANALOG_LOW) {
        return -1;
    }
    if (analogReading > ANALOG_HIGH) {
        return 1;
    }
    return 0;
}

void loop() {
    currentTimeMillis = millis();

    if (currentTimeMillis - lastSerial > PING_INTERVAL) {
        OUT_SERIAL(VLCF("Ping"));
    }

    if (currentTimeMillis - lastOTO > DEBOUNCE_INTERVAL) {
        if (digitalRead(PIN_OTO) == LOW) {
            lastOTO = currentTimeMillis;
            OUT_SERIAL(VLCF("OTO"));
        }
    }

    // are any of the analog multiplexed buttons pressed?
    if (currentTimeMillis - lastTeam1 > DEBOUNCE_INTERVAL) {
        switch (interpretAnalogPin(PIN_TEAM1)) {
            case 1:
                OUT_SERIAL(VLCF("OR1"));
                lastTeam1 = currentTimeMillis;
                break;
            case -1:
                OUT_SERIAL(VLCF("TTO1"));
                lastTeam1 = currentTimeMillis;
                break;
            default:
                break;
        }
    }

    if (currentTimeMillis - lastTeamA > DEBOUNCE_INTERVAL) {
        switch (interpretAnalogPin(PIN_TEAMA)) {
            case 1:
                OUT_SERIAL(VLCF("OR2"));
                lastTeamA = currentTimeMillis;
                break;
            case -1:
                OUT_SERIAL(VLCF("TTO2"));
                lastTeamA = currentTimeMillis;
                break;
            default:
                break;
        }
    }

    if (currentTimeMillis - lastEndGo > DEBOUNCE_INTERVAL) {
        switch (interpretAnalogPin(PIN_ENDGO)) {
            case -1:
                OUT_SERIAL(VLCF("START"));
                lastEndGo = currentTimeMillis;
                break;
            case 1:
                OUT_SERIAL(VLCF("CALL"));
                lastEndGo = currentTimeMillis;
                break;
            default:
                break;

        }
    }

    if (lineReady) {
        lineReady = false;
        line.toLowerCase();

        if (line == "start") {
            ampControl(Whistle);
            g_mp3Player.add2q(playlistStart);
            speechQueue[0] = 0;
            debug(VLCF("start"));
            playMode = Single;
            beaconCancel();
        }

        if (line == "call") {
            ampControl(Whistle);
            g_mp3Player.add2q(playlistCalloff);
            speechQueue[0] = 0;
            debug(VLCF("call"));
            playMode = Multiple;
            beaconCancel();
        }

        if (line == "callshort") {
            ampControl(Whistle);
            g_mp3Player.add2q(playlistCalloffShort);
            speechQueue[0] = 0;
            debug(VLCF("call"));
            playMode = Multiple;
            beaconCancel();
        }

        if (line == "callledshort") {
            beaconNoise = true;
            ampControl(Whistle);
            g_mp3Player.add2q(playlistCalloffShort);
            speechQueue[0] = 0;
            debug(VLCF("call"));
            playMode = Multiple;
            beaconCancel();
        }

        if (line == "callled") {
            beaconNoise = true;
            ampControl(Whistle);
            g_mp3Player.add2q(playlistCalloff);
            speechQueue[0] = 0;
            debug(VLCF("call"));
            playMode = Multiple;
            beaconCancel();
        }

        if (line == "ends") {
            ampControl(Whistle);
            speechQueue[0] = 0;
            g_mp3Player.add2q(playlistEndTO);
            debug(VLCF("ends"));
            playMode = Single;
            beaconCancel();
        }

        if (line == "oryes") {
            ampControl(Speak);
            playMode = Single;
            g_mp3Player.add2q("OY");
            OUT_SERIAL("OR_OK");
            beaconCancel();
        }

        if (line == "orno") {
            ampControl(Speak);
            playMode = Single;
            g_mp3Player.add2q("ON");
            OUT_SERIAL("OR_OK");
            beaconCancel();
        }

        if (line == "gameover") {
            playMode = Single;
            ampControl(Whistle);
            g_mp3Player.add2q(playlistEndTO);
            strcpy(speechQueue, "GG");
            beaconCancel();
        }

        if (line == "fulltime") {
            playMode = Single;
            ampControl(Whistle);
            g_mp3Player.add2q(playlistEndTO);
            strcpy(speechQueue, "FT");
            beaconCancel();
        }

        if (line == "halftime") {
            playMode = Single;
            ampControl(Whistle);
            g_mp3Player.add2q(playlistEndTO);
            strcpy(speechQueue, "HT");
            beaconCancel();
        }

        if (line == "whatareyou") {
            OUT_SERIAL(VLCF("IAm Whistle"));
        }

        if (line.startsWith("volw")) {
            int idx = line.lastIndexOf(' ');
            if (idx >= 0) {
                String volume = line.substring(idx);
                volume.trim();
                uint8_t newVolume = volume.toInt();
                if (newVolume > 30) {
                    newVolume = 30;
                }
                if (newVolume < 0) {
                    newVolume = 0;
                }
                OUT_SERIAL(VLCF("VOLW OK "), String(newVolume));
                EEPROM.update(0x00, newVolume);
                volumeWhistle = newVolume;
            }
            else {
                OUT_SERIAL(VLCF("VOLW BAD"));
            }
        }

        if (line.startsWith("vols")) {
            int idx = line.lastIndexOf(' ');
            if (idx >= 0) {
                String volume = line.substring(idx);
                volume.trim();
                uint8_t newVolume = volume.toInt();
                if (newVolume > 30) {
                    newVolume = 30;
                }
                if (newVolume < 0) {
                    newVolume = 0;
                }
                OUT_SERIAL(VLCF("VOLS OK "), String(newVolume));
                EEPROM.update(0x01, newVolume);
                volumeSpeak = newVolume;
            }
            else {
                OUT_SERIAL(VLCF("VOLS BAD"));
            }
        }

        if (line.startsWith("oto")) {
            // "official timeout"
            debug(VLCF("OTO"));
            strcpy(speechQueue, "OT");
            // mute horn, unmute whistle:
            ampControl(Whistle);
            g_mp3Player.add2q(playlistTO);
            playMode = Single;
            beaconCancel();
        }

        if (line.startsWith("tto ")) {
            // "team timeout: COLOUR"
            debug(VLCF("TTO"));
            strcpy(speechQueue, "TT");
            int idx = line.lastIndexOf(' ');
            if (idx >= 0) {
                String colour = line.substring(idx);
                colour.trim();
                catColour(speechQueue, colour);
            }
            // mute horn, unmute whistle:
            ampControl(Whistle);
            g_mp3Player.add2q(playlistTO);
            playMode = Single;
            delay(250);
            beaconCancel();
        }

        if (line.startsWith("or ")) {
            // "official review: COLOUR"
            debug(VLCF("OR"));
            strcpy(speechQueue, "OR");
            int idx = line.lastIndexOf(' ');
            if (idx >= 0) {
                String colour = line.substring(idx);
                colour.trim();
                catColour(speechQueue, colour);
            }
            // mute horn, unmute whistle:
            debug(speechQueue);
            ampControl(Whistle);
            g_mp3Player.add2q(playlistTO);
            playMode = Single;
            beaconCancel();
        }

        if (line.startsWith("count")) {
            debug(VLCF("Count"));
            ampControl(Speak);
            g_mp3Player.add2q("5S");
            playMode = Single;

            speechQueue[0] = 0;
            if (line == "countoff") {
                countType = All;
                countTime = millis();
            }

            if (line == "countdown") {
                countType = Down;
                countTime = millis();
            }

            if (line == "countup") {
                countType = Up;
                countTime = millis();
            }

            if (line == "countoneup") {
                countTime = millis();
                countType = OneUp;
            }

            if (line == "countonedown") {
                countTime = millis();
                countType = OneDown;
            }

            if (countType == NA) {
                OUT_SERIAL(VLCF("COUNT ERR"));
            }
        }
        line = "";
    }

    if (countType != NA) {
        long theTime = millis() - countTime;
        uint8_t bits = 0;
        switch (countType) {
            case Down:
                bits = theTime <= 5000 ? 1 : 0;
                bits |= theTime <= 4000 ? (1 << 1) : 0;
                bits |= theTime <= 3000 ? (1 << 2) : 0;
                bits |= theTime <= 2000 ? (1 << 3) : 0;
                bits |= theTime <= 1000 ? (1 << 4) : 0;
                break;
            case Up:
                bits = 1;
                bits |= theTime >= 1000 ? (1 << 1) : 0;
                bits |= theTime >= 2000 ? (1 << 2) : 0;
                bits |= theTime >= 3000 ? (1 << 3) : 0;
                bits |= theTime >= 4000 ? (1 << 4) : 0;
                break;
            case OneUp:
                bits = 1 << theTime/1000;
                break;
            case OneDown:
                bits = 1 << 4 - (theTime/1000);
                break;
            case All:
                bits = 0x1F;
                break;
        }
        if (theTime >= 5000) {
            debug(VLCF("About done"));
            timingBeacon.write(0);
            countType = NA;
        } else {
            timingBeacon.write(bits);
        }
    }

    g_mp3Player.run();

    switch (playMode) {
        case Single:
            if (g_mp3Player.isIdle(true)) {
                // is Single proceeded by Multiple?
                int len = strlen(speechQueue);
                if (len > 0) {
                    ampControl(Speak);
                    playMode = Multiple;
                    g_mp3Player.add2q(speechQueue);
                    speechQueue[0] = 0;
                } else {
                    playMode = None;
                    if (countType == NA) {
                        ampControl(Neither);
                        OUT_SERIAL(VLCF("DONE"));
                    }
                }
            }
            break;
        case Multiple:
            if (g_mp3Player.isIdle(false)) {
                playMode = None;
                if (countType == NA) {
                    ampControl(Neither);
                    OUT_SERIAL(VLCF("DONE"));
                }
            }
            break;
    }
    return;
}

void serialEvent() {
    while (Serial.available()) {
        char c = Serial.read();
        switch (c) {
            case '\n':
                lineReady = true;
            case '\r':
                break;
            default:
                line += c;
                if (line.length() > 30) {
                    line = "";
                }
                break;
        }
    }
}

/*
 * Putting various project notes here, even if it's not related to the Arduino-code.
 *
 * 1) Make sure the ESP-01S Rx Pin's pullup resistor is DISABLED
 * 2) If doing the voltage divider thing where buttons bypass resistors to send the analog pin high and low (thus 2 buttons
 *    on one pin), connect the high via a 1K or so resistor so that people pressing both buttons simultaneously
 *    doesn't cause a short circuit.
 * 3) The JQ8400 takes more than 100mS and less than 200mS to get going (by which we mean, the time between being told to play
 *    a file and the busy pin going high)
 *
 */