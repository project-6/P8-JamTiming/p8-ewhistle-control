//
// Created by vlchung on 1/9/21.
//

#include <Arduino.h>
#include "global.h"

long currentTimeMillis = 0;
long lastSerial = 0;

uint16_t dialRead(uint8_t analogChannelNumber, long maxChoices)
{
    // read from analog and convert into an int ranging between 0-maxChoices. The calculation is a bit tricky:
    // 1: We divide the "output space" into twice as many ranges (half-ranges) as there are actual switch positions
    // 2: Then assign "ranges" of output space into corresponding values based on 2 half-ranges per value
    // 3: The very first half-range is reserved as 0 (i.e. anything under 1023/2*(Pins-1) is considered 0
    // 4: Every other value has 1 half-range BELOW its theoretical value and 1 half-range ABOVE its theoretical value.

    // 2020-05-11: We need longs in here to avoid overflowing when maxChoices is large.
#define ADC_MAX 1023L // the max value the ADC will give
    const long CHOICES = maxChoices;
    const long OFFSET = ( (ADC_MAX / (CHOICES - 1L) ) / 2L );

    long reading = analogRead(analogChannelNumber);
    long value = reading - OFFSET;
    if (value < 0)
        value = 0;
    else
    {
        value /= (2*OFFSET);
        value += 1;
    }
    return value;
}

// Debug / output stuff that is important for everyone:
void debug(String s)
{
    // a bit of a hack, but designed to allow the USB port to also act as an interface to the system by prepending with "#"
    Serial.print(VLCF("# "));
    Serial.println(s);
    lastSerial = currentTimeMillis;
    return;
}

void debug(String s, String s2) // designed to allow the beginning of the message to be F() based and the rest assembled by code...
{
    Serial.print(VLCF("# "));
    Serial.print(s);
    Serial.println(s2);
    lastSerial = currentTimeMillis;
}

void OUT_SERIAL(String s)
{
    // a bit of a hack, but designed to allow the USB port to also act as an interface to the system by prepending with "!"
    Serial.print(VLCF("! "));
    Serial.println(s);
    lastSerial = currentTimeMillis;
}

void OUT_SERIAL(String s, String t)
{
    // to facilitate the storage of string constants in code memory, rather than copied from code into
    // ram at bootup
    Serial.print(VLCF("! "));
    Serial.print(s);
    Serial.println(t);
    lastSerial = currentTimeMillis;
}
