#include <Arduino.h>
#include "mp3player.h"
#include "global.h"
#include "JQ8400_Serial.h"

// the fact that these values are similar is coincidental
#define MIN_IDLE_TIME 500
// there is a 200mS lag between starting playback and the busy pin going high
#define MIN_START_TIME 200

// FIXME: rework this into the jam timer version of the ZH directory on the player module.

MP3Player::MP3Player(JQ8400_Serial *mp3) {
    mMP3 = mp3;
}

MP3Player::~MP3Player() {
}

void MP3Player::begin(uint8_t vol, uint8_t anaPin) {
    mMP3->setVolume(vol);
    mIdlePin = anaPin;
    return;
}

void MP3Player::add2q(const char *c) {
    mMP3->playSequenceByFlatFileName(c);
    mPlayStart = currentTimeMillis;
    mIdleStart = 0;
}

void MP3Player::run() {
    uint16_t pin = analogRead(mIdlePin);
    bool reading = pin > 300;
    if (reading) {
        mIdleStart = 0;
    } else {
        if ((currentTimeMillis - mPlayStart) > MIN_START_TIME) {
            if (mIdleStart == 0) {
                mIdleStart = currentTimeMillis;
            }
        }
    }
    return;
}

bool MP3Player::isIdle(bool immediate) {
    // immediate is useful if you know you are only playing a single track
    // not immediate is useful because when you are playing tracks in a sequence, the busy pin goes idle briefly between files
    if (currentTimeMillis - mPlayStart < MIN_START_TIME) {
        return false;
    }

    if (mIdleStart == 0) {
        return false;
    }

    if (immediate) {
        return mIdleStart > 0;
    } else {
        return (currentTimeMillis - mIdleStart) > MIN_IDLE_TIME;
    }
}

void MP3Player::setVol(int vol) {
    mMP3->setVolume(vol);
}