#ifndef MP3
#define MP3

#define MP3_MAX_Q_LEN 10

#include "JQ8400_Serial.h"
//#include "config.h"

/*
 * Following the realisation that I didn't spend much time stress testing the DFplayer prior to having a bit of a break, I've concluded it's not a good choice.
 *
 * Mainly because all the suppliers are actually supplying knock-offs that are quite unreliable, even with a move from "nth file written to SDcard" to
 * file 001.* in folder 01/
 *
 * We are now trying the JQ8600. This has some advantages, most notably the fact that it has on-board storage, and importantly in this context,
 * a sequence can be sent to the device, which will then manage playback for us. This makes the busy pin less important.
 *
 * On the downside, the documentation suggests that the busy pin voltage levels don't quite line up with what the Arduino is expecting, so
 * we use "DIO16" as "A2", and if analogRead(2) < 200 then the device is free. We also need to make sure that occurs for at least 200mS, because
 * the busy pin goes low IN-BETWEEN files.
 */

class MP3Player {
    public:
        MP3Player(JQ8400_Serial *mp3);
        ~MP3Player();
        void begin(uint8_t volume=29, uint8_t anaPin=A3);
        // starts / initialises the mp3 player; specify which DIO pin the
        // "Busy" (active high - not quite TTL voltage levels) pin is connected to on the Arduino.
        void run(); // should be run once per event loop (more or less)
        void add2q(const char *c);
        bool isIdle(bool immediate);
        void incVol(bool up);
        void setVol(int vol);
    private:
        void execute_CMD(uint8_t CMD, uint8_t Par1, uint8_t Par2);
        JQ8400_Serial *mMP3;
        long tmp1;
        long mIdleStart; // time the busy pin first went low -> we wait 200mS because the busy pin goes low in between words
        long tmpA;
        long mPlayStart;
        long tmpB;
        uint8_t mIdlePin;
};

#endif //MP3