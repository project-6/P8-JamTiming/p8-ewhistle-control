# python3 speech.py -l en-GB -v B
# Uses Google DeepMind Speech Synthesizer, language en-GB, voice B


01 - call off (once)
02 - start jam
03 - rolling whistle
04 - silence 150mS
5s - "5 seconds"
OT - "official timeout"
OR - "official review"
TT - "Team Timeout"
FT - "Full Time - awaiting final score declaration"
GG - "Official score has been declared. Game over"
HT - "half time"
00 - "Jet 3 beacon is online"
ON - official review denied
OY - official review retained
# The colour codes reflect the same letters used for p6, but start with 'C'
CA black
CB white
CC green
CD gold
CE red
CF blue
CG yellow
CH maroon
CI pink
CJ purple
CK celeste
CL orange
CM brown
CN teal

