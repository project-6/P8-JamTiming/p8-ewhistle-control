//
// Created by vlchung on 26/9/20.
//
// A very simple implementation of the MCP23017 protocol that covers what I consider the to be "the basics",
// with the Wire device being a #define so that we can use Wire.h and TinyWireM.h interchangeably. The two
// I2C Interfaces have different ancestors, which was causing problems building an ATTINY85 version of
// https://github.com/blemasle/arduino-mcp23017/

#include <Arduino.h>
#include "SimpleMCP23008.h"
#include "global.h"

#ifdef ARDUINO_attiny
    #include <TinyWireM.h>
    #define TheWire TinyWireM
    #define TINYWIRE
#else
    #include <Wire.h>
    #define TheWire Wire
    #undef TINYWIRE
#endif


SimpleMCP23008::SimpleMCP23008(uint8_t address) {
    mAddress = address;
}

void SimpleMCP23008::begin() {
    TheWire.begin();
    return;
}

SimpleMCP23008::~SimpleMCP23008() {
    return;
}

void SimpleMCP23008::mySend(uint8_t byte){
#ifdef TINYWIRE
    TheWire.send(byte);
#else
    TheWire.write(byte);
#endif
}

void SimpleMCP23008::setDDR(uint8_t ddr) {
    mDDR = ddr;
    TheWire.beginTransmission(mAddress);
    mySend(0x00); // IODIR
    mySend(ddr);
    TheWire.endTransmission();
    return;
}

void SimpleMCP23008::setInversion(uint8_t inv) {
    TheWire.beginTransmission(mAddress);
    mySend(0x01); // IPOL
    mySend(inv);
    TheWire.endTransmission();
    return;
}

void SimpleMCP23008::setPullUps(uint8_t pullUps) {
    TheWire.beginTransmission(mAddress);
    mySend(0x06); // CPPU
    mySend(pullUps);
    TheWire.endTransmission();
    return;
}

uint8_t SimpleMCP23008::read() {
    TheWire.beginTransmission(mAddress);
    mySend(0x09); // GPIO
    TheWire.endTransmission();
    TheWire.requestFrom(mAddress, 1);
    uint8_t port = TheWire.read();
    return port;
}

void SimpleMCP23008::write(uint8_t value) {
    // according to the documentation, writing to the GPIO port will sensibly effect OLAT[AB]
    TheWire.beginTransmission(mAddress);
    mySend(0x09); // GPIO
    mySend(value);
    TheWire.endTransmission();
    return;
}
